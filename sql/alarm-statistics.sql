
--------------------------------  alarm statistics -----------------------------------------
create database alarm_statistics on cluster ckcluster_2shards_2replicas;

create table alarm_statistics.cl_alarm_statistics_sub on cluster ckcluster_2shards_2replicas
(
    liftNo         String,
    alarmType      Int32,
    alarmCount     Int32,
    statisticsDate Date
) engine = ReplacingMergeTree()
      order by (statisticsDate, liftNo, alarmType)
      partition by toYYYYMM(statisticsDate)
;


CREATE table alarm_statistics.cl_alarm_statistics on cluster ckcluster_2shards_2replicas as alarm_statistics.cl_alarm_statistics_sub
engine = Distributed(ckcluster_2shards_2replicas,alarm_statistics,cl_alarm_statistics_sub,javaHash(liftNo));

create table alarm_statistics.alarm_statistics_reader as alarm_statistics.cl_alarm_statistics_sub engine = Kafka()
    SETTINGS
        kafka_broker_list = '192.168.4.16:9092,192.168.4.154:9092,192.168.4.215:9092',
        kafka_topic_list = 'alarm_type_statistics_topic',
        kafka_group_name = 'kafka_clickhouse_consumer_group',
        kafka_format = 'JSONEachRow',
        kafka_commit_every_batch = 1000
;

create MATERIALIZED view alarm_statistics.alarm_statistics_writer to alarm_statistics.cl_alarm_statistics as
select *
from alarm_statistics.alarm_statistics_reader asr;

----------- alarm from mysql -------------------------
create table alarm_statistics.cl_alarm_info_sub on cluster ckcluster_2shards_2replicas
(
    `id`             Int32 NOT NULL,
    alarmNo          String,
    liftNo           String,
    authNo           String,
    nbhdGuid         Int32,
    embeddedSn       String,
    standardType     Int32,
    alarmType        Int32,
    alarmLevel       Int8,
    sleepy           Int8,
    online           Int8,
    status           Int8,
    createTime       datetime,
    alarmDatetime    datetime,
    alarmEndDatetime datetime,
    handleDateTime   datetime,
    handler          String,
    handleStatus     Int8,
    ytStatus         String
)
    engine = MergeTree()
        order by (nbhdGuid, alarmDatetime, alarmType, liftNo);

create table alarm_statistics.cl_alarm_info on cluster ckcluster_2shards_2replicas as alarm_statistics.cl_alarm_info_sub
    engine = Distributed(ckcluster_2shards_2replicas, alarm_statistics, cl_alarm_info_sub,
             javaHash(liftNo) + alarmType);


create table alarm_statistics.cl_alarm_reader as alarm_statistics.cl_alarm_info engine = Kafka()
    SETTINGS
        kafka_broker_list = '192.168.2.66:9092,192.168.2.15:9092,192.168.2.92:9092',
        kafka_topic_list = 'alarm_migration_topic',
        kafka_group_name = 'migration_consumer_group_3',
        kafka_format = 'JSONEachRow',
        kafka_commit_every_batch = 100
;

create MATERIALIZED view alarm_statistics.cl_alarm_writer to alarm_statistics.cl_alarm_info as
select *
from alarm_statistics.cl_alarm_reader car



