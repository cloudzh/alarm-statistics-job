package com.xzl.flink;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static com.xzl.rs.ddl.CreateTable.MYSQL_ALARM_SOURCE_TEMPLATE;

public class TestSomething {


    @Test
    public void test_data_duration() {
        LocalDate startDate = LocalDate.of(2021,11,11);
        LocalDate endDate = LocalDate.now().minusDays(1);

        while (startDate.isBefore(endDate)){

            System.out.println(startDate.format(DateTimeFormatter.ofPattern("yyyyMMdd"))+":"+endDate.format(DateTimeFormatter.ofPattern("yyyyMMdd")));

            startDate = startDate.plusDays(1);

        }
    }

    @Test
    public void test() {
        System.out.println(String.format(MYSQL_ALARM_SOURCE_TEMPLATE,20211111,20211111
        ));
    }

}
