package com.xzl.rs.source.support;

import com.obs.services.model.ObsObject;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.List;

@Slf4j
class HuaweiObsProviderTest {

    HuaweiObsProvider provider = new HuaweiObsProvider("JNAH2LXNM3DVREMLRO73", "dYk8Vz6JRb0UofDwaoHs18yqXICauHTnaoUGKS8V", "obs.cn-east-2.myhuaweicloud.com", "formal-xzlobs-data-mining", "alarm");

    @Test
    void showFiles() {

        List<ObsObject> list = provider.showFiles();
        list.forEach(o -> log.info("{}", o));
    }

    @Test
    void downloadFile() {
    }

    @Test
    void downloadFileWithCheckPoint() {
    }
}