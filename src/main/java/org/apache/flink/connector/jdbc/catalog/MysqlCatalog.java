package org.apache.flink.connector.jdbc.catalog;

import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.TableSchema;
import org.apache.flink.table.api.constraints.UniqueConstraint;
import org.apache.flink.table.catalog.*;
import org.apache.flink.table.catalog.exceptions.CatalogException;
import org.apache.flink.table.catalog.exceptions.DatabaseNotExistException;
import org.apache.flink.table.catalog.exceptions.TableNotExistException;
import org.apache.flink.table.types.DataType;

import java.sql.*;
import java.util.*;

import static org.apache.flink.connector.jdbc.table.JdbcDynamicTableFactory.*;
import static org.apache.flink.table.factories.FactoryUtil.CONNECTOR;

@Slf4j
public class MysqlCatalog extends AbstractJdbcCatalog {
    public MysqlCatalog(String catalogName, String defaultDatabase, String username, String pwd, String baseUrl) {
        super(catalogName, defaultDatabase, username, pwd, baseUrl);
    }

    private static final Set<String> builtinDatabases = Sets.newHashSet("information_schema", "mysql", "performance_schema", "sys");

    @Override
    public List<String> listDatabases() throws CatalogException {
        List<String> databases = new ArrayList<>();
        ResultSet rs = null;
        try (Connection conn = DriverManager.getConnection(defaultUrl + "?useSSL=false", username, pwd)) {
            rs = conn.createStatement().executeQuery("show databases");
            while (rs.next()) {
                String dbName = rs.getString(1);
                if (!builtinDatabases.contains(dbName)) databases.add(dbName);
            }

            return databases;
        } catch (SQLException e) {
            throw new CatalogException(
                    String.format("Failed listing database in catalog %s", getName()), e);
        } finally {
            if (!Objects.isNull(rs)) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public CatalogDatabase getDatabase(String s) throws DatabaseNotExistException, CatalogException {
        log.info("database : {}", s);
        if (databaseExists(s))
            return new CatalogDatabaseImpl(Collections.emptyMap(), null);

        throw new DatabaseNotExistException(getName(), s);
    }

    @Override
    public List<String> listTables(String s) throws DatabaseNotExistException, CatalogException {
        if (!databaseExists(s)) {
            throw new DatabaseNotExistException(getName(), s);
        }
        List<String> tables = new ArrayList<>();
        ResultSet rs = null;
        try (Connection conn = DriverManager.getConnection(defaultUrl + "?useSSL=false", username, pwd)) {

            PreparedStatement stmt = conn.prepareStatement("select table_name from information_schema.TABLES where table_schema = ?");
            stmt.setString(1, s);

            rs = stmt.executeQuery();

            while (rs.next()) {
                tables.add(String.format("%s.%s.%s", s, s, rs.getString(1)));
            }

            return tables;
        } catch (SQLException e) {
            throw new CatalogException(
                    String.format("Failed listing database in catalog %s", getName()), e);
        } finally {
            if (!Objects.isNull(rs)) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public CatalogBaseTable getTable(ObjectPath tablePath) throws TableNotExistException, CatalogException {
        if (!tableExists(tablePath)) {
            throw new TableNotExistException(getName(), tablePath);
        }
        String dbUrl = baseUrl + tablePath.getDatabaseName() + "?useSSL=false";
        try (Connection conn = DriverManager.getConnection(dbUrl, username, pwd)) {
            DatabaseMetaData metaData = conn.getMetaData();
            Optional<UniqueConstraint> primaryKey = getPrimaryKey(metaData, tablePath.getDatabaseName(), tablePath.getObjectName());


            PreparedStatement ps = conn.prepareStatement(String.format("select * from %s", tablePath.getFullName()));

            ResultSetMetaData rsmd = ps.getMetaData();
            String[] names = new String[rsmd.getColumnCount()];
            DataType[] types = new DataType[rsmd.getColumnCount()];

            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                names[i - 1] = rsmd.getColumnName(i);
                types[i - 1] = fromJdbcType(rsmd, i);
                if (rsmd.isNullable(i) == ResultSetMetaData.columnNoNulls) {
                    types[i - 1] = types[i - 1].notNull();
                }
            }

            TableSchema.Builder tableBuilder = new TableSchema.Builder()
                    .fields(names, types);
            primaryKey.ifPresent(pk ->
                    tableBuilder.primaryKey(pk.getName(), pk.getColumns().toArray(new String[0]))
            );
            TableSchema tableSchema = tableBuilder.build();

            Map<String, String> props = new HashMap<>();
            props.put(CONNECTOR.key(), IDENTIFIER);
//            props.put(URL.key(), dbUrl);
//            props.put(TABLE_NAME.key(), tablePath.getObjectName());
//            props.put(USERNAME.key(), username);
//            props.put(PASSWORD.key(), pwd);

            return new CatalogTableImpl(
                    tableSchema,
                    props,
                    ""
            );

        } catch (SQLException e) {
            throw new CatalogException(
                    String.format("Failed getting table %s", tablePath.getFullName()), e);
        }
    }


    public static final String MYSQL_INT = "int";
    public static final String MYSQL_TINYINT = "tinyint";
    public static final String MYSQL_MEDIUMINT = "mediumint";
    public static final String MYSQL_SMALLINT = "smallint";
    public static final String MYSQL_INTEGER = "integer";
    public static final String MYSQL_BIGINT = "bigint";
    public static final String MYSQL_FLOAT = "float";
    public static final String MYSQL_DOUBLE = "double";
    public static final String MYSQL_DECIMAL = "decimal";
    public static final String MYSQL_TIMESTAMP = "timestamp";
    public static final String MYSQL_DATE = "date";
    public static final String MYSQL_DATETIME = "datetime";
    public static final String MSQL_VARCHAR = "varchar";
    public static final String MYSQL_TIME = "time";
    public static final String MYSQL_TEXT = "text";
    public static final String MYSQL_CHAR = "char";

    private DataType fromJdbcType(ResultSetMetaData metaData, int index) throws SQLException {
        String type = metaData.getColumnTypeName(index);

        switch (type.toLowerCase()) {
            case MYSQL_INT:
            case MYSQL_MEDIUMINT:
            case MYSQL_INTEGER:
            case MYSQL_BIGINT:
                return DataTypes.INT();
            case MYSQL_TINYINT:
                return DataTypes.TINYINT();
            case MYSQL_SMALLINT:
                return DataTypes.SMALLINT();
            case MYSQL_FLOAT:
                return DataTypes.FLOAT();
            case MYSQL_DOUBLE:
                return DataTypes.DOUBLE();
            case MYSQL_DECIMAL:
                return DataTypes.DECIMAL(metaData.getPrecision(index), metaData.getScale(index));
            case MYSQL_TIMESTAMP:
                return DataTypes.TIMESTAMP();
            case MYSQL_DATE:
            case MYSQL_DATETIME:
                return DataTypes.DATE();
            case MYSQL_TIME:
                return DataTypes.TIME();
            case MYSQL_TEXT:
            case MSQL_VARCHAR:
                return DataTypes.STRING();
            case MYSQL_CHAR:
                return DataTypes.CHAR(metaData.getPrecision(index));
            default:
                throw new UnsupportedOperationException(
                        String.format("Doesn't support Postgres type '%s' yet", type));
        }
    }

    @Override
    public boolean tableExists(ObjectPath objectPath) throws CatalogException {
        log.info("{} , {} , {}", objectPath.getDatabaseName(), objectPath.getObjectName(), objectPath.getFullName());

        ResultSet rs = null;
        try (Connection conn = DriverManager.getConnection(defaultUrl + "?useSSL=false", username, pwd)) {
            PreparedStatement pstm = conn.prepareStatement("select table_name from information_schema.TABLES where table_schema = ? and table_name = ?");
            pstm.setString(1, objectPath.getDatabaseName());
            pstm.setString(2, objectPath.getObjectName());

            rs = pstm.executeQuery();

            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (!Objects.isNull(rs)) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }
}
