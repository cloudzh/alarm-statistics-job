package org.apache.flink.connector.jdbc.catalog;

import org.apache.flink.connector.jdbc.dialect.JdbcDialect;
import org.apache.flink.connector.jdbc.dialect.JdbcDialects;
import org.apache.flink.connector.jdbc.dialect.MySQLDialect;
import org.apache.flink.connector.jdbc.dialect.PostgresDialect;

import static org.apache.flink.util.Preconditions.checkArgument;

public class JdbcCatalogUtils {

    public static void validateJdbcUrl(String url) {
        String[] parts = url.trim().split("\\/+");

        checkArgument(parts.length == 2);
    }

    public static AbstractJdbcCatalog createCatalog(String catalogName, String defaultDatabase, String username, String pwd, String baseUrl) {
        JdbcDialect dialect = JdbcDialects.get(baseUrl).get();

        if (dialect instanceof PostgresDialect) {
            return new PostgresCatalog(catalogName, defaultDatabase, username, pwd, baseUrl);
        } else if (dialect instanceof MySQLDialect) {
            return new MysqlCatalog(catalogName, defaultDatabase, username, pwd, baseUrl);
        } else {
            throw new UnsupportedOperationException(
                    String.format("Catalog for '%s' is not supported yet.", dialect)
            );
        }
    }
}
