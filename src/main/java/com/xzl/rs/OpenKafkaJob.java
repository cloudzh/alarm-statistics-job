package com.xzl.rs;

import com.xzl.rs.msg.OpenAlarm;
import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

public class OpenKafkaJob {

    private static final String CONFIG_TEMPLATE = "cdc-job-config-%s.properties";
    private static final String defaultProfile = "dev";

    private static final String CREATE_STATISTICS_UPSERT_KAFKA_SINK_TEMPLATE = "create table `alarm_statistics_upsert_sink` (\n" +
            "`liftNo` String,\n" +
            "`alarmType` int,\n" +
            "`alarmCount` bigint,\n" +
            "`statisticsDate` DATE,\n" +
            "primary key(liftNo,statisticsDate) not enforced \n" +
            ")with (\n" +
            "'connector' = 'upsert-kafka',\n" +
            "'topic' = '%s',\n" +
            "'properties.bootstrap.servers' = '%s', \n" +
            "'key.format'='json', \n" +
            "'value.format'='json', \n" +
            "'value.fields-include' = 'ALL' \n" +
            ")";

//    private static final String CREATE_OPEN_ALARM_KAFKA_SOURCE_TEMPLATE = "";

    public static void main(String[] args) {
        String config = String.format(CONFIG_TEMPLATE, defaultProfile);

        if (!Objects.isNull(args) && args.length > 0) {
            config = String.format(CONFIG_TEMPLATE, args[0]);
        }

        try (InputStream input = AlarmCountStatisticsJob.class.getClassLoader().getResourceAsStream(config)) {
            ParameterTool tool = ParameterTool.fromPropertiesFile(input);
            String statisticsTopic = tool.get("statistics.sink.kafka.topic");
            String statisticsKafkaAddress = tool.get("statistics.sink.kafka.address");
            String alarmKafkaTopic = tool.get("alarm.sink.kafka.topic");
            String alarmKafkaAddress = tool.get("alarm.sink.kafka.address");
            String scanModel = tool.get("cdc.scan.startup.model");
            String cdcSourceDatabase = tool.get("cdc.mysql.source.database");
            String cdcSourceIp = tool.get("cdc.mysql.source.ip");
            String cdcSourcePort = tool.get("cdc.mysql.source.port");
            String cdcSourceUsername = tool.get("cdc.mysql.source.username");
            String cdcSourcePassword = tool.get("cdc.mysql.source.password");
            String cdcSourceTablePattern = tool.get("cdc.mysql.source.table-pattern");
            String checkpointStoragePath = tool.get("flink.checkpoint.storage.path");


            KafkaSource<OpenAlarm> openAlarmSource = KafkaSource.<OpenAlarm>builder()
                    .setBootstrapServers(statisticsKafkaAddress)
                    .setTopics("send_alarm_to_open_topic")
                    .setGroupId("flink_open_alarm_consumer")
                    .setStartingOffsets(OffsetsInitializer.latest())
                    .setValueOnlyDeserializer(new DeserializationSchema<>() {

                        private ObjectMapper mapper = new ObjectMapper();

                        @Override
                        public OpenAlarm deserialize(byte[] message) throws IOException {
                            return mapper.readValue(message, OpenAlarm.class);
                        }

                        @Override
                        public boolean isEndOfStream(OpenAlarm nextElement) {
                            return Objects.isNull(nextElement);
                        }

                        @Override
                        public TypeInformation<OpenAlarm> getProducedType() {
                            return TypeInformation.of(OpenAlarm.class);
                        }
                    })
                    .build();


        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }
    }
}
