package com.xzl.rs.source.support;

import com.obs.services.ObsClient;
import com.obs.services.exception.ObsException;
import com.obs.services.model.*;

import java.util.List;

public class HuaweiObsProvider {

    private static final ThreadLocal<Integer> RETRY_TIMES = new ThreadLocal<>();
    private static final int MAX_RETRY_TIMES = 3;

    private String dir = "alarm/";
    //    private String endPoint = "obs.cn-east-2.myhuaweicloud.com";
    private String bucketName = "formal-xzlobs-data-mining";
//    private String ak = "JNAH2LXNM3DVREMLRO73";
//    private String sk = "dYk8Vz6JRb0UofDwaoHs18yqXICauHTnaoUGKS8V";


    private ObsClient client;


    public HuaweiObsProvider(String ak, String sk, String endPoint, String bucketName, String dir) {
        client = new ObsClient(ak, sk, endPoint);
        this.dir = dir;
        this.bucketName = bucketName;
    }

    public List showFiles() {
        String realDir = dir.lastIndexOf('/') > 0 ? dir : dir + "/";
        ListObjectsRequest req = new ListObjectsRequest(bucketName);
        req.setPrefix(realDir);
        ObjectListing listing = client.listObjects(req);

        return listing.getObjects();
    }

    public void downloadFile(String fileName) {
        ObsObject obsObject = client.getObject(bucketName, fileName);

    }

    public void downloadFileWithCheckPoint(String fileName) {
        DownloadFileRequest req = new DownloadFileRequest(bucketName, fileName);
        req.setDownloadFile(""); // download to local directory
        req.setTaskNum(5);
        req.setPartSize(10 * 1024 * 1024);
        req.setEnableCheckpoint(true);

        try {
            DownloadFileResult result = client.downloadFile(req);
        } catch (ObsException e) {
            if (MAX_RETRY_TIMES <= RETRY_TIMES.get()) return;
            downloadFileWithCheckPoint(fileName);
        }

    }

    private boolean canTry() {
        return false;
    }

}
