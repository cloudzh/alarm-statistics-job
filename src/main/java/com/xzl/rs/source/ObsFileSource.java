package com.xzl.rs.source;


import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.connector.source.*;
import org.apache.flink.core.io.SimpleVersionedSerializer;


@Slf4j
public class ObsFileSource implements Source {

    private static final long serialVersionUID = -6806825596460438075L;

    private String ak;
    private String sk;
    private String endpoint;
    private String bucketName;
    private String dir;


    ObsFileSource(String ak, String sk, String endpoint, String bucketName, String dir) {
        this.ak = ak;
        this.sk = sk;
        this.endpoint = endpoint;
        this.bucketName = bucketName;
        this.dir = dir;
    }

    public static ObsFileSourceBuilder builder() {
        return new ObsFileSourceBuilder();
    }

    /**
     * Get the boundedness of this source.
     *
     * @return the boundedness of this source.
     */
    @Override
    public Boundedness getBoundedness() {
        return Boundedness.CONTINUOUS_UNBOUNDED;
    }

    /**
     * Creates a new reader to read data from the splits it gets assigned. The reader starts fresh
     * and does not have any state to resume.
     *
     * @param readerContext The {@link SourceReaderContext context} for the source reader.
     * @return A new SourceReader.
     * @throws Exception The implementor is free to forward all exceptions directly. Exceptions
     *                   thrown from this method cause task failure/recovery.
     */
    @Override
    public SourceReader createReader(SourceReaderContext readerContext) throws Exception {
        log.warn("create reader...........");
        return null;
    }

    /**
     * Creates a new SplitEnumerator for this source, starting a new input.
     *
     * @param enumContext The {@link SplitEnumeratorContext context} for the split enumerator.
     * @return A new SplitEnumerator.
     * @throws Exception The implementor is free to forward all exceptions directly. * Exceptions
     *                   thrown from this method cause JobManager failure/recovery.
     */
    @Override
    public SplitEnumerator createEnumerator(SplitEnumeratorContext enumContext) throws Exception {
        log.warn("create enumerator...........");
        return null;
    }

    /**
     * Restores an enumerator from a checkpoint.
     *
     * @param enumContext The {@link SplitEnumeratorContext context} for the restored split
     *                    enumerator.
     * @param checkpoint  The checkpoint to restore the SplitEnumerator from.
     * @return A SplitEnumerator restored from the given checkpoint.
     * @throws Exception The implementor is free to forward all exceptions directly. * Exceptions
     *                   thrown from this method cause JobManager failure/recovery.
     */
    @Override
    public SplitEnumerator restoreEnumerator(SplitEnumeratorContext enumContext, Object checkpoint) throws Exception {
        log.warn("restore enumerator..............");
        return null;
    }

    /**
     * Creates a serializer for the source splits. Splits are serialized when sending them from
     * enumerator to reader, and when checkpointing the reader's current state.
     *
     * @return The serializer for the split type.
     */
    @Override
    public SimpleVersionedSerializer getSplitSerializer() {
        log.warn("get split serializer");
        return null;
    }

    /**
     * Creates the serializer for the {@link SplitEnumerator} checkpoint. The serializer is used for
     * the result of the {@link SplitEnumerator#snapshotState()} method.
     *
     * @return The serializer for the SplitEnumerator checkpoint.
     */
    @Override
    public SimpleVersionedSerializer getEnumeratorCheckpointSerializer() {
        log.warn("get Enumerator checkpoint serializer");
        return null;
    }
}
