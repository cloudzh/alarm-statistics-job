package com.xzl.rs.source;


import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ObsFileSourceBuilder {

    private String ak;
    private String sk;
    private String endpoint;
    private String bucketName;
    private String dir;

    ObsFileSourceBuilder() {

    }

    public ObsFileSourceBuilder ak(String ak) {
        this.ak = ak;
        return this;
    }

    public ObsFileSourceBuilder sk(String sk) {
        this.sk = sk;
        return this;
    }

    public ObsFileSourceBuilder endpoint(String endpoint) {
        this.endpoint = endpoint;
        return this;
    }

    public ObsFileSourceBuilder bucketName(String bucketName) {
        this.bucketName = bucketName;
        return this;
    }

    public ObsFileSourceBuilder dir(String dir) {
        this.dir = dir;
        return this;
    }

    public ObsFileSource build() {
        check();
        return new ObsFileSource(ak, sk, endpoint, bucketName, dir);
    }

    private void check() {
        if (Strings.isNullOrEmpty(ak) ||
                Strings.isNullOrEmpty(sk) ||
                Strings.isNullOrEmpty(endpoint) ||
                Strings.isNullOrEmpty(bucketName) ||
                Strings.isNullOrEmpty(dir)) throw new RuntimeException("Lack Args");
    }
}
