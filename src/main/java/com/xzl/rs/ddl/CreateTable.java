package com.xzl.rs.ddl;

public interface CreateTable {

    String CREATE_CDC_TABLE = "create table  cl_alarm_info\n" +
            "(\n" +
            "    database_name String metadata virtual, \n" +
            "    table_name String metadata virtual, \n" +
            "    `id`               INT NOT NULL,\n" +
            "    alarmNo          String,\n" +
            "    liftNo           String,\n" +
            "    authNo           String,\n" +
            "    nbhdGuid         int,\n" +
            "    embeddedSn       String,\n" +
            "    standardType     int,\n" +
            "    alarmType        int,\n" +
            "    alarmLevel       int,\n" +
            "    sleepy           int,\n" +
            "    online           int,\n" +
            "    status           int,\n" +
            "    createTime       timestamp,\n" +
            "    alarmDatetime    timestamp,\n" +
            "    alarmEndDatetime timestamp,\n" +
            "    handleDateTime   timestamp,\n" +
            "    handler          String,\n" +
            "    handleStatus     int,\n" +
            "    ytStatus         String,\n" +
            "    PRIMARY KEY (`id`) NOT ENFORCED\n" +
            ") with (\n" +
            "      'connector' = 'mysql-cdc',\n" +
//            "      'hostname' = '101.35.245.42',\n" +
//            "      'port' = '3306',\n" +
//            "      'username' = 'root',\n" +
//            "      'password' = 'root',\n" +
//            "      'database-name' = 'alarm_info',\n" +

            "      'hostname' = '192.168.2.33',\n" +
            "      'port' = '3306',\n" +
            "      'username' = 'root',\n" +
            "      'password' = 'Xzl@K8sBASEserver!',\n" +
            "      'database-name' = 'dt_alarm',\n" +

//            "      'hostname' = '101.35.245.42',\n" +
//            "      'port' = '3306',\n" +
//            "      'username' = 'root',\n" +
//            "      'password' = 'root',\n" +
//            "      'database-name' = 'alarm_info',\n" +
            "      'table-name' = 'cl_alarm_info_202[1-9][0-9]*',\n" +
            "      'connect.timeout' = '3000000' , \n" +
            "      'scan.startup.mode'='initial',\n" +
            "      'debezium.max.queue.size' = '300' , \n" +
            "      'debezium.max.batch.size' = '3000' , \n" +
            "      'debezium.poll.interval.ms' = '1000' \n" +
//            "      'debezium.timeout' = '300' , \n" +
            "      )";

    String CREATE_STATISTICS_PRINTER = "create table alarm_statistics_printer (\n" +
            "    liftNo String,\n" +
            "    alarmType int,\n" +
            "    alarmCount bigint ,\n" +
            "    statisticsDate DATE\n" +
            ") with ('connector' = 'print')";

    String CREATE_STATISTICS_WIth_TABLE_NAME_PRINTER = "create table alarm_statistics_with_table_printer (\n" +
            "    table_name String,\n" +
            "    liftNo String,\n" +
            "    alarmType int,\n" +
            "    alarmDate DATE ,\n" +
            "    alarmCount bigint\n" +
            ") with ('connector' = 'print')";

    @Deprecated
    String CREATE_STATISTICS_KAFKA_SINK = "create table `alarm_statistics_sink` (\n" +
            "`liftNo` String,\n" +
            "`alarmType` int,\n" +
            "`alarmCount` bigint,\n" +
            "`statisticsDate` DATE\n" +
            ")with (\n" +
            "'connector' = 'kafka',\n" +
            "'topic' = 'alarm_type_statistics_topic',\n" +
            "'properties.bootstrap.servers' = 'kafka.cloud.tencent.com:9092','format'='json' \n" +
            ")";

    String CREATE_STATISTICS_UPSERT_KAFKA_SINK = "create table `alarm_statistics_upsert_sink` (\n" +
            "`liftNo` String,\n" +
            "`alarmType` int,\n" +
            "`alarmCount` bigint,\n" +
            "`statisticsDate` DATE,\n" +
            "primary key(liftNo,statisticsDate) not enforced \n" +
            ")with (\n" +
            "'connector' = 'upsert-kafka',\n" +
            "'topic' = 'alarm_type_statistics_topic',\n" +
//            "'properties.bootstrap.servers' = 'kafka.cloud.tencent.com:9092', \n" + //dev
            "'properties.bootstrap.servers' = '192.168.2.66:9092,192.168.2.15:9092,192.168.2.92:9092', \n" + //test
//            "'properties.bootstrap.servers' = '192.168.4.16:9092,192.168.4.154:9092,192.168.4.215:9092', \n" + //prod
            "'key.format'='json', \n" +
            "'value.format'='json', \n" +
            "'value.fields-include' = 'ALL' \n" +
            ")";

    @Deprecated
    String CREATE_MIGRATION_SINK = "create table alarm_migration_sink\n" +
            "(\n" +
            "    `id`             INT NOT NULL,\n" +
            "    alarmNo          String,\n" +
            "    liftNo           String,\n" +
            "    authNo           String,\n" +
            "    embeddedSn       String,\n" +
            "    standardType     int,\n" +
            "    alarmType        int,\n" +
            "    alarmLevel       int,\n" +
            "    sleepy           int,\n" +
            "    online           int,\n" +
            "    status           int,\n" +
            "    createTime       timestamp,\n" +
            "    alarmDatetime    timestamp,\n" +
            "    alarmEndDatetime timestamp,\n" +
            "    handleDateTime   timestamp,\n" +
            "    handler          String,\n" +
            "    handleStatus     int,\n" +
            "    ytStatus         String\n" +
            ") with (\n" +
            "    'connector' = 'kafka',\n" +
            "    'topic' = 'alarm_migration_topic',\n" +
//            "'properties.bootstrap.servers' = 'kafka.cloud.tencent.com:9092', \n" + //dev
            "'properties.bootstrap.servers' = '192.168.2.66:9092,192.168.2.15:9092,192.168.2.92:9092', \n" + //test
//            "'properties.bootstrap.servers' = '192.168.4.16:9092,192.168.4.154:9092,192.168.4.215:9092', \n" + //prod
            "    'format'='json'\n" +
            ")";

    String CREATE_MIGRATION_UPSERT_SINK = "create table alarm_migration_sink\n" +
            "(\n" +
            "    `id`             INT NOT NULL,\n" +
            "    alarmNo          String,\n" +
            "    liftNo           String,\n" +
            "    authNo           String,\n" +
            "    nbhdGuid         int,\n" +
            "    embeddedSn       String,\n" +
            "    standardType     int,\n" +
            "    alarmType        int,\n" +
            "    alarmLevel       int,\n" +
            "    sleepy           int,\n" +
            "    online           int,\n" +
            "    status           int,\n" +
            "    createTime       timestamp,\n" +
            "    alarmDatetime    timestamp,\n" +
            "    alarmEndDatetime timestamp,\n" +
            "    handleDateTime   timestamp,\n" +
            "    handler          String,\n" +
            "    handleStatus     int,\n" +
            "    ytStatus         String,\n" +
            "    PRIMARY KEY (`id`) NOT ENFORCED\n" +
            ") with (\n" +
            "    'connector' = 'upsert-kafka',\n" +
            "    'topic' = 'alarm_migration_topic',\n" +
//            "'properties.bootstrap.servers' = 'kafka.cloud.tencent.com:9092', \n" + //dev
            "'properties.bootstrap.servers' = '192.168.2.66:9092,192.168.2.15:9092,192.168.2.92:9092', \n" + //test
//            "'properties.bootstrap.servers' = '192.168.4.16:9092,192.168.4.154:9092,192.168.4.215:9092', \n" + //prod
            "    'value.format'='json' ,\n" +
            "    'key.format'='json', \n" +
            "    'value.fields-include' = 'ALL' \n" +
            ")";
    String CREATE_MIGRATION_SINK_LOG = "create table alarm_migration_log  with ('connector'='printer') like alarm_migration_sink";

    @Deprecated
    String MYSQL_ALARM_SOURCE_TEMPLATE = "CREATE TABLE `alarmInfo%s` (\n" +
            "  `id` int NOT NULL  ,\n" +
            "  `alarmNo` String NOT NULL ,\n" +
            "  `liftNo` String  NULL ,\n" +
            "  `authNo` String  NULL ,\n" +
            "  `embeddedSn` String  NULL ,\n" +
            "  `standardType` int  NULL ,\n" +
            "  `alarmType` int  NULL,\n" +
            "  `alarmLevel` int  NULL,\n" +
            "  `sleepy` tinyint  NULL,\n" +
            "  `online` tinyint  NULL ,\n" +
            "  `status` tinyint  NULL,\n" +
            "  `createTime` timestamp  NULL ,\n" +
            "  `alarmDatetime` timestamp  NULL ,\n" +
            "  `alarmEndDatetime` timestamp  NULL ,\n" +
            "  `handleDateTime` timestamp  NULL ,\n" +
            "  `handler` String  NULL ,\n" +
            "  `handleStatus` tinyint,\n" +
            "  `ytStatus` String  NULL ,\n" +
            "  `videoStatus` tinyint  ,\n" +
            "  PRIMARY KEY (`id`) NOT ENFORCED\n" +
            ") with (\n" +
            "  'connector' = 'jdbc',\n" +
            "  'url' = 'jdbc:mysql://101.35.245.42:3306/alarm_info',\n" +
            "  'table-name' = 'cl_alarm_info_%s',\n" +
            "  'username' = 'root',\n" +
            "  'password' = 'root',\n" +
            "  'scan.fetch-size' = '1000', \n" +
//                "  'scan.partition.column' = 'alarmType',\n" +
//                "  'scan.partition.num' = '9',\n" +
            "  'lookup.max-retries' = '3' \n" +
            ")";


}
