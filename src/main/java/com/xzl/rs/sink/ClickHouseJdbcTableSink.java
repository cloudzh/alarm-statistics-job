package com.xzl.rs.sink;

import org.apache.flink.connector.jdbc.JdbcStatementBuilder;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

public class ClickHouseJdbcTableSink implements JdbcStatementBuilder<Map<String, Object>> {

    private static final long serialVersionUID = 7560111690150935431L;

    @Override
    public void accept(PreparedStatement preparedStatement, Map<String, Object> stringObjectMap) throws SQLException {
        preparedStatement.setString(1, (String) stringObjectMap.get(""));
        preparedStatement.setString(2, String.valueOf(stringObjectMap.get("")));
        preparedStatement.setInt(3, (Integer) stringObjectMap.get(""));
        preparedStatement.setDate(4, (Date) stringObjectMap.get(""));
    }
}
