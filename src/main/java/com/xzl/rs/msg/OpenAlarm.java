package com.xzl.rs.msg;

import lombok.Data;

import java.util.Map;

@Data
public class OpenAlarm {

    private String alarmSerialNo;
    private String warnLevel;
    private String liftNo;
    private String streetCode;
    private String nbhdGuid;
    private String registerCode;
    private String standardType;
    private String embeddedSN;
    private String warnCode;
    private String originWarnCode;
    private String warnCodeName;
    private String sleepy;

    private String handleStatus;
    private String status;
    private String ytStatus;
    private String currentStatus;

    private String sendDatetime;
    private String handleDatetime;
    private String warnDatetime;
    private String warnEndDatetime;
    private String platform;

    private Map<String,Object> extention;

}
