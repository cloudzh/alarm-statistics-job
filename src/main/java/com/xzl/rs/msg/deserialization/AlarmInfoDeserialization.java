package com.xzl.rs.msg.deserialization;

import com.xzl.rs.msg.AlarmInfo;
import org.apache.flink.api.common.serialization.AbstractDeserializationSchema;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.DeserializationFeature;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;


public class AlarmInfoDeserialization extends AbstractDeserializationSchema<AlarmInfo> {
    private static final Logger log = LoggerFactory.getLogger(AlarmInfoDeserialization.class);

    private static ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
        mapper.configure(DeserializationFeature.FAIL_ON_NUMBERS_FOR_ENUMS, false);
    }

    @Override
    public AlarmInfo deserialize(byte[] message) throws IOException {
        String msgStr = new String(message, Charset.forName("utf-8"));
        try {
            AlarmInfo alarmInfo = mapper.readValue(msgStr, AlarmInfo.class);
            log.info("Message Is {},Json Format {}", msgStr, alarmInfo);
            return alarmInfo;
        } catch (IllegalArgumentException e) {
            log.error("AlarmMessageDeserialization Error", e);
        }
        return null;
    }
}
