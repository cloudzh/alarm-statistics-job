//package com.xzl.rs.msg.deserialization;
//
////import com.xzl.rs.msg.RawAlarm;
////import com.xzl.rs.msg.RawAlarmProtos;
//import org.apache.flink.api.common.serialization.AbstractDeserializationSchema;
//
//import java.io.IOException;
//import java.time.Instant;
//import java.time.LocalDateTime;
//import java.time.ZoneId;
//
//public class RawAlarmDeserialization extends AbstractDeserializationSchema<RawAlarm> {
//    @Override
//    public RawAlarm deserialize(byte[] message) throws IOException {
//        RawAlarmProtos.PushData pushData = RawAlarmProtos.PushData.parseFrom(message);
//
//        RawAlarm rawAlarm = new RawAlarm();
//        if (pushData.getDataType() == 3) {
//            RawAlarmProtos.Alarm raise = RawAlarmProtos.Alarm.parseFrom(pushData.getData());
//            rawAlarm.setOnline(raise.getOnline());
//            rawAlarm.setDeviceCode(raise.getDeviceId());
//            rawAlarm.setAlarmType(raise.getAlarmType());
//            rawAlarm.setLiftNo(raise.getLiftId());
//            rawAlarm.setAlarmDatetime(LocalDateTime.ofInstant(Instant.ofEpochMilli(raise.getTime()), ZoneId.systemDefault()));
////            rawAlarm.setAlarmDate(LocalDateTime.ofInstant(Instant.ofEpochMilli(raise.getTime()), ZoneId.systemDefault()));
//            rawAlarm.setStatus(1);
//        } else {
//            rawAlarm.setStatus(0);
//        }
//        return rawAlarm;
//    }
//}
