package com.xzl.rs.msg.sink;


import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.xzl.rs.msg.StatisticsSource;
import lombok.Data;

import java.time.LocalDate;

@Data
public class AlarmStatistics {

    private String liftNo;
    private LocalDate statisticsDate;
    private Integer alarmType;
    private Integer alarmCount = 0;


    public AlarmStatistics() {

    }


    public static AlarmStatistics convertFrom(StatisticsSource source, Integer alarmCount) {
        AlarmStatistics statistics = new AlarmStatistics();
        statistics.setStatisticsDate(source.getStatisticsDate());
        statistics.setAlarmCount(alarmCount);
        statistics.setLiftNo(source.getLiftNo());
        statistics.setAlarmType(source.getAlarmType());
        return statistics;
    }


    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("liftNo", liftNo)
                .add("statisticsTime", statisticsDate)
                .add("alarmType", alarmType)
                .add("alarmCount", alarmCount)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AlarmStatistics that = (AlarmStatistics) o;
        return Objects.equal(liftNo, that.liftNo) && Objects.equal(statisticsDate, that.statisticsDate) && Objects.equal(alarmType, that.alarmType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(liftNo, statisticsDate, alarmType);
    }

}
