package com.xzl.rs.msg;

import com.google.common.base.Objects;
import lombok.Data;
import org.apache.flink.types.Row;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class StatisticsSource {

    private String liftNo;
    private Integer alarmType;
    private LocalDate statisticsDate;
    private LocalDateTime dataTime;


    public static StatisticsSource convertFrom(Row row) {
        StatisticsSource source = new StatisticsSource();
        source.setAlarmType((Integer) row.getField("alarmType"));
        source.setLiftNo((String) row.getField("liftNo"));
        source.setStatisticsDate((LocalDate) row.getField("alarmDate"));
        source.setDataTime((LocalDateTime) row.getField("alarmDatetime"));
        return source;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatisticsSource source = (StatisticsSource) o;
        return Objects.equal(liftNo, source.liftNo) && Objects.equal(alarmType, source.alarmType) && Objects.equal(statisticsDate, source.statisticsDate);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(liftNo, alarmType, statisticsDate);
    }
}
