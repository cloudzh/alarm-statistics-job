package com.xzl.rs.msg;

public class RawAlarm extends BaseAlarm {

    private String deviceCode;//设备编码

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }
}
