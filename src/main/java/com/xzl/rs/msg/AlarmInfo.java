package com.xzl.rs.msg;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

public class AlarmInfo extends BaseAlarm {

    private String alarmSerialNo;
    private String embeddedSN;
    //    private String sleepy;
    @JsonProperty("warnDatetime")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime warnEndDatetime;

    public String getAlarmSerialNo() {
        return alarmSerialNo;
    }

    public void setAlarmSerialNo(String alarmSerialNo) {
        this.alarmSerialNo = alarmSerialNo;
    }

    public String getEmbeddedSN() {
        return embeddedSN;
    }

    public void setEmbeddedSN(String embeddedSN) {
        this.embeddedSN = embeddedSN;
    }

    public LocalDateTime getWarnEndDatetime() {
        return warnEndDatetime;
    }

    public void setWarnEndDatetime(LocalDateTime warnEndDatetime) {
        this.warnEndDatetime = warnEndDatetime;
    }

}
