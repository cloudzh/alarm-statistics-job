package com.xzl.rs.msg;

import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.util.Date;

public class BaseAlarm {


    @JsonProperty("warnCode")
    private int alarmType;
    private String liftNo;

    @JsonProperty("warnDatetime")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime alarmDatetime;

    private int online;//是否在线 0：离线 1：在线
    private int status; //6：自动结束；7：超期结束；

    public int getAlarmType() {
        return alarmType;
    }

    public void setAlarmType(int alarmType) {
        this.alarmType = alarmType;
    }

    public String getLiftNo() {
        return liftNo;
    }

    public void setLiftNo(String liftNo) {
        this.liftNo = liftNo;
    }

    public LocalDateTime getAlarmDatetime() {
        return alarmDatetime;
    }

    public void setAlarmDatetime(LocalDateTime alarmDatetime) {
        this.alarmDatetime = alarmDatetime;
    }

    public int getOnline() {
        return online;
    }

    public void setOnline(int online) {
        this.online = online;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
