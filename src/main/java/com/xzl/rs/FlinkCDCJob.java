package com.xzl.rs;

import com.ververica.cdc.connectors.mysql.source.MySqlSource;
import com.xzl.rs.msg.StatisticsSource;
import com.xzl.rs.msg.sink.AlarmStatistics;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.runtime.state.storage.FileSystemCheckpointStorage;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.Schema;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;

import static org.apache.flink.table.api.Expressions.$;

public class FlinkCDCJob {

    private static final String CONFIG_TEMPLATE = "cdc-job-config-%s.properties";
    private static final String defaultProfile = "dev";

    private static final String CREATE_STATISTICS_UPSERT_KAFKA_SINK_TEMPLATE = "create table `alarm_statistics_upsert_sink` (\n" +
            "`liftNo` String,\n" +
            "`alarmType` int,\n" +
            "`alarmCount` bigint,\n" +
            "`statisticsDate` DATE,\n" +
            "primary key(liftNo,statisticsDate,alarmType) not enforced \n" +
            ")with (\n" +
            "'connector' = 'upsert-kafka',\n" +
            "'topic' = '%s',\n" +
            "'properties.bootstrap.servers' = '%s', \n" +
            "'key.format'='json', \n" +
            "'value.format'='json', \n" +
            "'value.fields-include' = 'ALL' \n" +
            ")";

    private static final String CREATE_MIGRATION_UPSERT_SINK_TEMPLATE = "create table alarm_migration_sink\n" +
            "(\n" +
            "    `id`             INT NOT NULL,\n" +
            "    alarmNo          String,\n" +
            "    liftNo           String,\n" +
            "    authNo           String,\n" +
            "    nbhdGuid         int,\n" +
            "    embeddedSn       String,\n" +
            "    standardType     int,\n" +
            "    alarmType        int,\n" +
            "    alarmLevel       int,\n" +
            "    sleepy           int,\n" +
            "    online           int,\n" +
            "    status           int,\n" +
            "    createTime       timestamp,\n" +
            "    alarmDatetime    timestamp,\n" +
            "    alarmEndDatetime timestamp,\n" +
            "    handleDateTime   timestamp,\n" +
            "    handler          String,\n" +
            "    handleStatus     int,\n" +
            "    ytStatus         String,\n" +
            "    PRIMARY KEY (`id`) NOT ENFORCED\n" +
            ") with (\n" +
            "    'connector' = 'upsert-kafka',\n" +
            "    'topic' = '%s',\n" +
            "'properties.bootstrap.servers' = '%s', \n" + //test
            "    'value.format'='json' ,\n" +
            "    'key.format'='json', \n" +
            "    'value.fields-include' = 'ALL' \n" +
            ")";


    private static final String CREATE_CDC_TABLE_TEMPLATE = "create table  cl_alarm_info\n" +
            "(\n" +
            "    database_name String metadata virtual, \n" +
            "    table_name String metadata virtual, \n" +
            "    `id`               INT NOT NULL,\n" +
            "    alarmNo          String,\n" +
            "    liftNo           String,\n" +
            "    authNo           String,\n" +
            "    nbhdGuid         int,\n" +
            "    embeddedSn       String,\n" +
            "    standardType     int,\n" +
            "    alarmType        int,\n" +
            "    alarmLevel       int,\n" +
            "    sleepy           int,\n" +
            "    online           int,\n" +
            "    status           int,\n" +
            "    createTime       timestamp,\n" +
            "    alarmDatetime    timestamp(3),\n" +
            "    watermark for alarmDatetime as alarmDatetime - interval '30' minute,\n" +
            "    alarmEndDatetime timestamp,\n" +
            "    handleDateTime   timestamp,\n" +
            "    handler          String,\n" +
            "    handleStatus     int,\n" +
            "    ytStatus         String,\n" +
            "    PRIMARY KEY (`id`) NOT ENFORCED\n" +
            ") with (\n" +
            "      'connector' = 'mysql-cdc',\n" +
            "      'hostname' = '%s',\n" +
            "      'port' = '%s',\n" +
            "      'username' = '%s',\n" +
            "      'password' = '%s',\n" +
            "      'database-name' = '%s',\n" +
            "      'table-name' = '%s',\n" +
//            "      'server-id' = '66666-99999' , \n" +
            "      'connect.timeout' = '3000000' , \n" +
            "      'scan.startup.mode'='latest-offset',\n" +
            "      'scan.incremental.snapshot.enabled'='false',\n" +
//            "      'debezium.max.queue.size' = '3000' , \n" +
            "      'debezium.snapshot.locking.mode' = 'none' , \n" +
//            "      'debezium.max.batch.size' = '5000' , \n" +
            "      'scan.startup.specific-offset.file' = 'mysql-bin.018956' , \n" +
            "      'scan.startup.specific-offset.pos' = '40784989' , \n" +
            "      'server-time-zone' = 'Asia/Shanghai' , \n" +
            "      'debezium.poll.interval.ms' = '1000' \n" +
            "      )";


    // Attention： only work on flink v1.13
    public static void main(String[] args) {
        String config = String.format(CONFIG_TEMPLATE, defaultProfile);

        if (!Objects.isNull(args) && args.length > 0) {
            config = String.format(CONFIG_TEMPLATE, args[0]);
        }

        try (InputStream input = AlarmCountStatisticsJob.class.getClassLoader().getResourceAsStream(config)) {
            ParameterTool tool = ParameterTool.fromPropertiesFile(input);
            String statisticsTopic = tool.get("statistics.sink.kafka.topic");
            String statisticsKafkaAddress = tool.get("statistics.sink.kafka.address");
            String alarmKafkaTopic = tool.get("alarm.sink.kafka.topic");
            String alarmKafkaAddress = tool.get("alarm.sink.kafka.address");
            String scanModel = tool.get("cdc.scan.startup.model");
            String cdcSourceDatabase = tool.get("cdc.mysql.source.database");
            String cdcSourceIp = tool.get("cdc.mysql.source.ip");
            String cdcSourcePort = tool.get("cdc.mysql.source.port");
            String cdcSourceUsername = tool.get("cdc.mysql.source.username");
            String cdcSourcePassword = tool.get("cdc.mysql.source.password");
            String cdcSourceTablePattern = tool.get("cdc.mysql.source.table-pattern");
            String checkpointStoragePath = tool.get("flink.checkpoint.storage.path");

            EnvironmentSettings settings = EnvironmentSettings.newInstance().useBlinkPlanner().inStreamingMode().build();

            Configuration configuration = new Configuration();
            configuration.setString("execution.checkpointing.interval", "30s");
            configuration.setString("execution.checkpointing.tolerable-failed-checkpoints", "100");
            configuration.setString("restart-strategy", "fixed-delay");
            configuration.setInteger("restart-strategy.fixed-delay.attempts", 2147483647);

            StreamExecutionEnvironment streamEnv = StreamExecutionEnvironment.getExecutionEnvironment(configuration)
                    .setParallelism(1);

            streamEnv.enableCheckpointing(30 * 1000, CheckpointingMode.AT_LEAST_ONCE);
            streamEnv.getCheckpointConfig().setTolerableCheckpointFailureNumber(100);
            streamEnv.getCheckpointConfig().setCheckpointStorage(new FileSystemCheckpointStorage(checkpointStoragePath));

            StreamTableEnvironment tableEnv = StreamTableEnvironment.create(streamEnv, settings);

//            Configuration tableEnvConfig = tableEnv.getConfig().getConfiguration();
//            tableEnvConfig.setString("table.exec.mini-batch.enabled", "true"); // local-global aggregation depends on mini-batch is enabled
//            tableEnvConfig.setString("table.exec.mini-batch.allow-latency", "5 s");
//            tableEnvConfig.setString("table.exec.mini-batch.size", "5000");
//            tableEnvConfig.setString("table.optimizer.agg-phase-strategy", "TWO_PHASE");

            String format = String.format(CREATE_CDC_TABLE_TEMPLATE, cdcSourceIp, cdcSourcePort
                    , cdcSourceUsername, cdcSourcePassword,
                    cdcSourceDatabase, cdcSourceTablePattern);
            tableEnv.executeSql(format);

            tableEnv.executeSql(String.format(CREATE_STATISTICS_UPSERT_KAFKA_SINK_TEMPLATE, statisticsTopic, statisticsKafkaAddress));

            Table alarmTable = tableEnv.from("cl_alarm_info");

//            tableEnv.executeSql(String.format(CREATE_MIGRATION_UPSERT_SINK_TEMPLATE, alarmKafkaTopic, alarmKafkaAddress));
//            tableEnv.executeSql("insert into alarm_migration_sink select id,alarmNo,liftNo,authNo,nbhdGuid,embeddedSn,standardType,alarmType,alarmLevel,sleepy,online,status,createTime,alarmDatetime,alarmEndDatetime,handleDatetime,handler,handleStatus,ytStatus from cl_alarm_info");

//            alarmTable.select(
//                            $("id"), $("alarmNo"), $("liftNo"),
//                            $("authNo"), $("nbhdGuid"), $("embeddedSn"),
//                            $("standardType"), $("alarmType"), $("alarmLevel"),
//                            $("sleepy"), $("online"), $("status"),
//                            $("createTime"), $("alarmDatetime"), $("alarmEndDatetime"),
//                            $("handleDateTime"), $("handler"), $("handleStatus"), $("ytStatus")
//                    )
//                    .executeInsert("alarm_migration_sink");

            Table countSourceTable = alarmTable
//                    .where($("alarmDatetime").isGreater(LocalDateTime.of(2022,1,17,23,59,59)))
                    .where($("ytStatus").isEqual("20"))
                    .where($("handleStatus").in(2, 4))
                    .where($("status").in(6, 7, 8))
                    .addColumns($("alarmDatetime").cast(DataTypes.DATE()).as("alarmDate"))
                    .select($("liftNo"), $("alarmType"), $("alarmDate"), $("alarmDatetime"));

            DataStream dataStream = tableEnv.toRetractStream(countSourceTable, Row.class)
                    .filter(row -> row.f0).map(row -> row.f1)
                    .map(StatisticsSource::convertFrom);

            dataStream.print();

            Table simpleSource = tableEnv.fromDataStream(dataStream,
                    Schema.newBuilder()
                            .column("liftNo", DataTypes.STRING())
                            .column("alarmType", DataTypes.INT())
                            .column("statisticsDate", DataTypes.DATE())
                            .column("dataTime", DataTypes.TIMESTAMP(3))
                            .watermark("dataTime", $("dataTime"))
                            .build()
            );


            tableEnv.createTemporaryView("cumulate_table", simpleSource.orderBy($("dataTime")));

            Table statisticsTable = tableEnv.sqlQuery("select liftNo,alarmType,count(alarmType) as alarmCount, statisticsDate  from Table(\n" +
                    "        CUMULATE(Table cumulate_table ,DESCRIPTOR(dataTime),INTERVAL '2' HOURS,INTERVAL '1' DAY)\n" +
                    "    ) group by window_start,window_end,liftNo,alarmType,statisticsDate ");

//            tableEnv.executeSql(CREATE_STATISTICS_PRINTER);
//            statisticsTable.executeInsert("alarm_statistics_printer");

            statisticsTable.executeInsert("alarm_statistics_upsert_sink");

            streamEnv.execute();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static ProcessFunction<List<AlarmStatistics>, AlarmStatistics> getProcessFunction() {
        return new ProcessFunction<>() {
            private static final long serialVersionUID = -6866737880025539265L;

            @Override
            public void processElement(List<AlarmStatistics> list, ProcessFunction<List<AlarmStatistics>, AlarmStatistics>.Context ctx, Collector<AlarmStatistics> out) throws Exception {
                list.forEach(v -> out.collect(v));
            }
        };
    }


    private static void tableApi() {
        MySqlSource source = MySqlSource.builder()
                .hostname("")
                .port(3306)
                .username("")
                .password("")
                .tableList("")
                .databaseList("")
                .build();
    }

}
