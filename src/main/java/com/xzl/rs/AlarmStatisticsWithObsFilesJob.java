package com.xzl.rs;

import com.xzl.rs.source.ObsFileSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

@Slf4j
public class AlarmStatisticsWithObsFilesJob {


    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        ObsFileSource.builder()
                .ak("JNAH2LXNM3DVREMLRO73")
                .sk("dYk8Vz6JRb0UofDwaoHs18yqXICauHTnaoUGKS8V")
                .endpoint("obs.cn-east-2.myhuaweicloud.com")
                .bucketName("formal-xzlobs-data-mining")
                .dir("alarm")
                .build();

        env.execute();
    }
}
