package com.xzl.rs.model;


import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor(staticName = "of")
public class AlarmTypeScoreRecordDetail {

    @NonNull
    private String liftNo; // 电梯编码
    @NonNull
    private Integer alarmType; // 告警类型
    @NonNull
    private Integer alarmNum; // 告警数量
    private Integer durationDays; // 持续天数
    private Double scores; // 分数 扣分=负 ； 加分= 正
    private Double tsid; // total scores in durations // 持续时间内的分数和

    public AlarmTypeScoreRecordDetail newDetail(Integer alarmNum, Integer durationDays, Double scores) {
        AlarmTypeScoreRecordDetail detail = new AlarmTypeScoreRecordDetail(getLiftNo(), getAlarmType(), alarmNum);
        detail.durationDays = durationDays;
        detail.scores = scores;

        detail.handleTSID(scores);
        return detail;
    }

    private void handleTSID(Double scores) {
        Double _tsid = scores > 0 ? scores : -scores;
        if (durationDays > 1) {
            this.tsid += _tsid;
        } else {
            this.tsid = _tsid;
        }
    }
}
