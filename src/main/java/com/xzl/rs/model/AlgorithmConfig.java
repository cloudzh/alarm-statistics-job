package com.xzl.rs.model;

import lombok.Data;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class AlgorithmConfig {

    private String deptNo; // 单位 （维保/监管）
    private Integer alarmType; // 告警类型

    private Integer reduction; // 基础扣分
    private Integer reductionTimes; // 最多扣分次数
    private Double reductionRate; // 扣分速率

    private Integer recoverPeriod; // 分数恢复周期


    public List<Integer> getConfigAlarmTypes(String deptNo) {
        return Collections.emptyList();
    }

    public Map<Integer,AlgorithmConfig> getConfig() {
        return new HashMap<>();
    }

}
