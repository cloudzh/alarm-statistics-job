package com.xzl.rs.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Data
public class LiftAlarmStatistics {

    private String liftNo;
    private String deptNo;
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private LocalDate statisticsDate;

    private String alarmType;
    private Integer count;

    private Map<String, Integer> alarmCountMap = new HashMap<>();

    public LiftAlarmStatistics init(List<LiftAlarmStatistics> alarmStatistics) {
        alarmStatistics.forEach(s -> alarmCountMap.put(s.getAlarmType(), s.getCount()));
        return this;
    }


    public Integer getAlarmCount(Integer alarmType) {
        return alarmCountMap.containsKey(alarmType) ? alarmCountMap.get(alarmType) : 0;
    }
}

