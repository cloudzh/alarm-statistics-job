package com.xzl.rs.model;

import lombok.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class LiftRunningState {

    @NonNull
    private String liftNo;
    @NonNull
    private LocalDate scoreDate; //评分日期
    @NonNull
    private AlgorithmConfig algorithmConfig;

    private Double lastScore; // 上次积分
    @Getter
    private Double currentScore; // 当前分数
    @Getter
    private Double reduceScores; // 本次扣分
    @Getter
    private Double recoverScores; // 本次加分

    @Getter
    private LiftRunningStateHistory lastHistory;

    public LiftRunningState compute(LiftAlarmStatistics statistics) {
        Map<Integer, AlgorithmConfig> algorithmMap = algorithmConfig.getConfig();

        List<AlarmTypeScoreRecordDetail> details = algorithmMap.entrySet().parallelStream()
                .map(entry ->
                        doCompute(
                                statistics.getAlarmCount(entry.getKey()),
                                entry.getValue(),
                                lastHistory.fetchOrCreate(statistics.getLiftNo(), entry.getKey(), statistics.getAlarmCount(entry.getKey())))
                )
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());

        Optional<LiftRunningStateHistory> optional = LiftRunningStateHistory.create(getLiftNo(), getScoreDate(), details);

        if (optional.isPresent()) {
            LiftRunningStateHistory lrsh = optional.get();
            this.currentScore = getLastScore() + lrsh.getRecoverScores() + lrsh.getReduceScores();
            this.recoverScores = lrsh.getRecoverScores();
            this.reduceScores = lrsh.getReduceScores();

            this.lastHistory = lrsh;
        } else {
            this.currentScore = getLastScore();
        }
        return this;
    }

    private Optional<AlarmTypeScoreRecordDetail> doCompute(Integer alarmCount, AlgorithmConfig config, AlarmTypeScoreRecordDetail historyDetail) {
        if (alarmCount > 0) { // reduce
            Integer basicReduction = config.getReduction();
            Integer times = config.getReductionTimes() <= alarmCount ? config.getReductionTimes() : alarmCount;
            Double rate = config.getReductionRate();
            Integer durationDays = historyDetail.getDurationDays() + 1;

            //TODO refactor
            Double v = (basicReduction * times) + rate * (durationDays - 1);

            return Optional.ofNullable(historyDetail.newDetail(alarmCount, durationDays, -v));
        } else { // recover
            if (!Objects.isNull(historyDetail)) {
                if (historyDetail.getScores() > 0) { // recover last time

                    if (config.getRecoverPeriod().intValue() > historyDetail.getDurationDays()) {
                        return Optional.ofNullable(historyDetail.newDetail(alarmCount, historyDetail.getDurationDays() + 1, historyDetail.getScores()));
                    }
                } else { // reduce last time
                    Double recoverScores = historyDetail.getTsid() / config.getRecoverPeriod();
                    return Optional.ofNullable(historyDetail.newDetail(alarmCount, 1, recoverScores));
                }

            }

            return Optional.empty();
        }
    }

}
