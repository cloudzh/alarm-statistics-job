package com.xzl.rs.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Getter
@RequiredArgsConstructor(staticName = "of")
public class LiftRunningStateHistory {

    @NonNull
    public String liftNo;
    @NonNull
    private LocalDate scoreDate;
    private Double reduceScores;
    private Double recoverScores;

    private List<AlarmTypeScoreRecordDetail> details;

    private Map<Integer, AlarmTypeScoreRecordDetail> detailMap = new HashMap<>();


    public LiftRunningStateHistory buildDetails(List<AlarmTypeScoreRecordDetail> details) {
        this.details = details;
        details.forEach(rshd -> detailMap.put(rshd.getAlarmType(), rshd));

        return this;
    }

    public AlarmTypeScoreRecordDetail fetchOrCreate(String liftNo, Integer alarmType, Integer alarmCount) {
        if (detailMap.containsKey(alarmType)) return detailMap.get(alarmType);

        return AlarmTypeScoreRecordDetail.of(liftNo, alarmType, alarmCount);
    }

    public static Optional<LiftRunningStateHistory> create(String liftNo, LocalDate scoreDate, List<AlarmTypeScoreRecordDetail> details) {
        if (null == details || details.isEmpty()) return Optional.empty();

        LiftRunningStateHistory history = new LiftRunningStateHistory(liftNo, scoreDate);
        history.computeScores(details);
        return Optional.of(history);
    }

    private LiftRunningStateHistory computeScores(List<AlarmTypeScoreRecordDetail> details) {

        var recoverScores = details.stream().filter(detail -> detail.getScores() > 0)
                .mapToDouble(detail -> new BigDecimal(detail.getScores()).doubleValue()).sum();
        var reduceScores = details.stream().filter(detail -> detail.getScores() < 0)
                .mapToDouble(detail -> new BigDecimal(detail.getScores()).doubleValue()).sum();

        this.recoverScores = recoverScores;
        this.reduceScores = reduceScores;

        this.details = details;
        return this;
    }

}
