package com.xzl.rs.function;

import org.apache.flink.table.catalog.DataTypeFactory;
import org.apache.flink.table.functions.FunctionKind;
import org.apache.flink.table.functions.UserDefinedFunction;
import org.apache.flink.table.types.inference.TypeInference;

public class JsonConverterFunction extends UserDefinedFunction {
    @Override
    public FunctionKind getKind() {
        return FunctionKind.OTHER;
    }

    @Override
    public TypeInference getTypeInference(DataTypeFactory dataTypeFactory) {
        return null;
    }
}
