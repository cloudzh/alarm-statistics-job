package com.xzl.rs.function;

import com.xzl.rs.msg.AlarmInfo;
import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.WindowAssigner;
import org.apache.flink.streaming.api.windowing.triggers.ProcessingTimeTrigger;
import org.apache.flink.streaming.api.windowing.triggers.Trigger;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;

public class DynamicTumblingEventTimeWindows extends WindowAssigner<Object, TimeWindow> {

    private static final Logger log = LoggerFactory.getLogger(DynamicTumblingEventTimeWindows.class);

    private Integer period = 5;


    public DynamicTumblingEventTimeWindows(Integer periodOfMinutes) {
        this.period = periodOfMinutes;
    }

    @Override
    public Collection<TimeWindow> assignWindows(Object element, long timestamp, WindowAssignerContext context) {

        AlarmInfo alarm = (AlarmInfo) element;
        LocalDateTime startTime = alarm.getAlarmDatetime();
        LocalDateTime endTime = startTime.plusMinutes(period);

        long startTimestamp = startTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        long endTimestamp = endTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();

        LocalDate startDate = startTime.toLocalDate();
        LocalDate endDate = endTime.toLocalDate();

        if (!startDate.isEqual(endDate)) {
            endTimestamp = LocalDateTime.of(startDate, LocalTime.of(23, 59, 59)).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        }

        log.info(" {}-{} DynamicTumblingEventTimeWindows StartTime: {},EndTime: {},Duration: {}",
                alarm.getLiftNo(),
                alarm.getAlarmType(),
                startTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
                LocalDateTime.ofInstant(Instant.ofEpochMilli(endTimestamp), ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
                period);

        return Collections.singleton(new TimeWindow(startTimestamp, endTimestamp));
    }

    @Override
    public Trigger<Object, TimeWindow> getDefaultTrigger(StreamExecutionEnvironment env) {
        return ProcessingTimeTrigger.create();
    }

    @Override
    public TypeSerializer<TimeWindow> getWindowSerializer(ExecutionConfig executionConfig) {
        return new TimeWindow.Serializer();
    }

    @Override
    public boolean isEventTime() {
        return false;
    }
}
