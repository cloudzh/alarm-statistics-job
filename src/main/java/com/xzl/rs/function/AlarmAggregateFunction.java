package com.xzl.rs.function;

import com.google.common.base.Strings;
import com.xzl.rs.msg.AlarmInfo;
import com.xzl.rs.msg.sink.AlarmStatistics;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class AlarmAggregateFunction implements AggregateFunction<AlarmInfo, AlarmStatistics, AlarmStatistics> {

    private static final Logger log = LoggerFactory.getLogger(AlarmAggregateFunction.class);

    @Override
    public AlarmStatistics createAccumulator() {
        log.info("create Accumulator.................");
        return new AlarmStatistics();
    }

    @Override
    public AlarmStatistics add(AlarmInfo value, AlarmStatistics accumulator) {

        if (Strings.isNullOrEmpty(accumulator.getLiftNo())) accumulator.setLiftNo(value.getLiftNo());
        if (Objects.isNull(accumulator.getAlarmType())) accumulator.setAlarmType(value.getAlarmType());
        accumulator.setStatisticsDate(value.getAlarmDatetime().toLocalDate());


        return accumulator;
    }

    @Override
    public AlarmStatistics getResult(AlarmStatistics accumulator) {

        log.info("AlarmStatistics : {}", accumulator);
        return accumulator;
    }

    @Override
    public AlarmStatistics merge(AlarmStatistics a, AlarmStatistics b) {

        log.info("{}-{} Before Merge , a: {},b: {}", a.getLiftNo(), a.getAlarmType(), a.getAlarmCount(), b.getAlarmCount());

        log.info("AfterMerge : {},merge data: {}", a, b.getAlarmCount());
        return a;
    }
}
