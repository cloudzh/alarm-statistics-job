package com.xzl.rs.function;

import com.google.common.collect.Iterables;
import com.xzl.rs.msg.sink.AlarmStatistics;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

import java.time.*;

public class AlarmStatisticsProcessWindowFunction extends ProcessWindowFunction<AlarmStatistics, AlarmStatistics, Integer, TimeWindow> {

    final OutputTag<AlarmStatistics> outputTag;

    public AlarmStatisticsProcessWindowFunction(OutputTag<AlarmStatistics> outputTag) {
        this.outputTag = outputTag;
    }

    @Override
    public void process(Integer key, ProcessWindowFunction<AlarmStatistics, AlarmStatistics, Integer, TimeWindow>.Context context, Iterable<AlarmStatistics> elements, Collector<AlarmStatistics> out) throws Exception {

        elements.forEach(e -> out.collect(e));

        TimeWindow window = context.window();

        long endDay = LocalDateTime.of(LocalDate.ofInstant(Instant.ofEpochMilli(window.getStart()), ZoneId.systemDefault()), LocalTime.of(23, 59, 59)).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();

        if (endDay == window.getEnd()) {
            context.output(outputTag, Iterables.getLast(elements));
        }
    }
}
