package com.xzl.rs.function;

import com.google.common.collect.Maps;
import com.xzl.rs.msg.StatisticsSource;
import com.xzl.rs.msg.sink.AlarmStatistics;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

@Slf4j
public class StatisticsAccumulator<T> {

    ConcurrentMap<T, Integer> concurrentHashMap = Maps.newConcurrentMap();


    public StatisticsAccumulator compute(T source) {

        if (concurrentHashMap.containsKey(source)) {
            concurrentHashMap.computeIfPresent(source, (key, oldValue) -> {
                if (source instanceof StatisticsSource) {
                    return oldValue + 1;
                } else {
                    return oldValue.intValue() + ((AlarmStatistics) source).getAlarmCount();
                }
            });
        } else {
            concurrentHashMap.put(source, source instanceof StatisticsSource ? 1 : ((AlarmStatistics) source).getAlarmCount());
        }

        return this;
    }

    public List<AlarmStatistics> getResults() {

        List<AlarmStatistics> statisticsList = concurrentHashMap.entrySet().stream().map(entry -> {
            if (entry.getKey() instanceof StatisticsSource) {
                return AlarmStatistics.convertFrom((StatisticsSource) entry.getKey(), entry.getValue());
            } else {
                AlarmStatistics statistics = (AlarmStatistics) entry.getKey();
                statistics.setAlarmCount(entry.getValue());

                return statistics;
            }
        }).collect(Collectors.toList());

        return statisticsList;
    }
}
