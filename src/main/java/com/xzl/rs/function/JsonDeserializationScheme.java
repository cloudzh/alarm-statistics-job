package com.xzl.rs.function;

import com.xzl.rs.model.LiftAlarmStatistics;
import org.apache.flink.api.common.serialization.AbstractDeserializationSchema;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JsonDeserializationScheme extends AbstractDeserializationSchema<LiftAlarmStatistics> {
    private static final long serialVersionUID = 1454840018328683173L;
    private final ObjectMapper mapper = new ObjectMapper();

    public JsonDeserializationScheme() {
    }

    @Override
    public LiftAlarmStatistics deserialize(byte[] message) throws IOException {
        return mapper.readValue(message, LiftAlarmStatistics.class);
    }
}
