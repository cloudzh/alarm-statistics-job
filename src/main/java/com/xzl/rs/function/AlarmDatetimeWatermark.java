package com.xzl.rs.function;

import com.xzl.rs.msg.AlarmInfo;
import org.apache.flink.api.common.eventtime.Watermark;
import org.apache.flink.api.common.eventtime.WatermarkGenerator;
import org.apache.flink.api.common.eventtime.WatermarkOutput;

import java.time.ZoneId;

public class AlarmDatetimeWatermark implements WatermarkGenerator<AlarmInfo> {

    private final int maxOutOfIgnore = 30 * 1000; // 30s

    private long currentMaxTimestamp;

    @Override
    public void onEvent(AlarmInfo event, long eventTimestamp, WatermarkOutput output) {

        currentMaxTimestamp = Math.max(event.getAlarmDatetime().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(), eventTimestamp);
    }

    @Override
    public void onPeriodicEmit(WatermarkOutput output) {
        output.emitWatermark(new Watermark(currentMaxTimestamp - maxOutOfIgnore - 1));
    }
}
