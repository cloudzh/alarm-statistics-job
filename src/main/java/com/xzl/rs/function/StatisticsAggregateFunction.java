package com.xzl.rs.function;

import com.xzl.rs.msg.sink.AlarmStatistics;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.AggregateFunction;

import java.util.List;

@Slf4j
public class StatisticsAggregateFunction<S> implements AggregateFunction<S, StatisticsAccumulator<S>, List<AlarmStatistics>> {
    /**
     * Creates a new accumulator, starting a new aggregate.
     *
     * <p>The new accumulator is typically meaningless unless a value is added via {@link
     * #add(S, StatisticsAccumulator)}.
     *
     * <p>The accumulator is the state of a running aggregation. When a program has multiple
     * aggregates in progress (such as per key and window), the state (per key and window) is the
     * size of the accumulator.
     *
     * @return A new accumulator, corresponding to an empty aggregate.
     */
    @Override
    public StatisticsAccumulator<S> createAccumulator() {
        return new StatisticsAccumulator();
    }

    /**
     * Adds the given input value to the given accumulator, returning the new accumulator value.
     *
     * <p>For efficiency, the input accumulator may be modified and returned.
     *
     * @param value       The value to add
     * @param accumulator The accumulator to add the value to
     * @return The accumulator with the updated state
     */
    @Override
    public StatisticsAccumulator<S> add(S value, StatisticsAccumulator<S> accumulator) {
        return accumulator.compute(value);
    }

    /**
     * Gets the result of the aggregation from the accumulator.
     *
     * @param accumulator The accumulator of the aggregation
     * @return The final aggregation result.
     */
    @Override
    public List<AlarmStatistics> getResult(StatisticsAccumulator<S> accumulator) {
        return accumulator.getResults();
    }

    /**
     * Merges two accumulators, returning an accumulator with the merged state.
     *
     * <p>This function may reuse any of the given accumulators as the target for the merge and
     * return that. The assumption is that the given accumulators will not be used any more after
     * having been passed to this function.
     *
     * @param a An accumulator to merge
     * @param b Another accumulator to merge
     * @return The accumulator with the merged state
     */
    @Override
    public StatisticsAccumulator merge(StatisticsAccumulator a, StatisticsAccumulator b) {
        log.error("StatisticsAccumulator Merge");
        return null;
    }
}
