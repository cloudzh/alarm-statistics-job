package com.xzl.rs.function;

import com.xzl.rs.model.LiftAlarmStatistics;
import com.xzl.rs.model.LiftRunningState;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

public class RunningStateProcessFunction extends KeyedProcessFunction<Integer, LiftAlarmStatistics, LiftRunningState> {

    @Override
    public void processElement(LiftAlarmStatistics value, KeyedProcessFunction<Integer, LiftAlarmStatistics, LiftRunningState>.Context ctx, Collector<LiftRunningState> out) throws Exception {
        System.out.println(value);
    }
}
