package com.xzl.rs.function;

import lombok.extern.slf4j.Slf4j;
import org.apache.flink.table.annotation.DataTypeHint;
import org.apache.flink.table.annotation.FunctionHint;
import org.apache.flink.table.api.*;
import org.apache.flink.table.functions.TableFunction;
import org.apache.flink.types.Row;

import static org.apache.flink.table.api.Expressions.$;

@Slf4j
@FunctionHint(output = @DataTypeHint("ROW<liftNo STRING>"))
public class DynamicTableFunction extends TableFunction<Row> {

    public void eval(String tableSuffix) {
        EnvironmentSettings settings = EnvironmentSettings.newInstance().useBlinkPlanner().inBatchMode().build();
        TableEnvironment env = TableEnvironment.create(settings);
//        TableResult result = env.executeSql("select liftNo,alarmType,ytStatus,handleStatus from alarm_storage.alarm_info.cl_alarm_info_" + tableSuffix);
        env.executeSql("create table `alarmTypeStatisticsKafkaSinkTable` (\n" +
                "`liftNo` String,\n" +
                "`alarmType` String,\n" +
                "`count` int,\n" +
                "`statisticsDate` DATE\n" +
                ")with (\n" +
                "'connector' = 'kafka',\n" +
                "'topic' = 'alarm_type_statistics_topic',\n" +
                "'properties.bootstrap.servers' = '101.35.245.42:9092','format'='json' \n" +
                ")");

        env.executeSql("CREATE TABLE `alarmInfo" + tableSuffix + "` (\n" +
                "  `id` int NOT NULL  ,\n" +
                "  `alarmNo` String NOT NULL ,\n" +
                "  `liftNo` String  NULL ,\n" +
                "  `authNo` String  NULL ,\n" +
                "  `embeddedSn` String  NULL ,\n" +
                "  `standardType` int  NULL ,\n" +
                "  `alarmType` int  NULL,\n" +
                "  `alarmLevel` int  NULL,\n" +
                "  `sleepy` tinyint  NULL,\n" +
                "  `online` tinyint  NULL ,\n" +
                "  `status` tinyint  NULL,\n" +
                "  `createTime` timestamp  NULL ,\n" +
                "  `alarmDatetime` timestamp  NULL ,\n" +
                "  `alarmEndDatetime` timestamp  NULL ,\n" +
                "  `handleDateTime` timestamp  NULL ,\n" +
                "  `handler` String  NULL ,\n" +
                "  `handleStatus` Int,\n" +
                "  `ytStatus` String  NULL ,\n" +
                "  `videoStatus` tinyint  ,\n" +
                "  PRIMARY KEY (`id`) NOT ENFORCED\n" +
                ") with (\n" +
                "  'connector' = 'jdbc',\n" +
                "  'url' = 'jdbc:mysql://101.35.245.42:3306/alarm_info?useSSL=false',\n" +
                "  'table-name' = 'cl_alarm_info_" + tableSuffix + "',\n" +
                "  'username' = 'root',\n" +
                "  'password' = 'root',\n" +
                "  'scan.fetch-size' = '100', \n" +
//                "  'scan.partition.column' = 'alarmType',\n" +
//                "  'scan.partition.num' = '9',\n" +
                "  'lookup.max-retries' = '3' \n" +
                ")");

        Table table = env.from("alarmInfo" + tableSuffix).where($("ytStatus").isEqual("20").and($("handleStatus").equals(4))).select($("liftNo"), $("alarmType"), $("alarmDatetime").cast(DataTypes.DATE()).as("statisticsDate"))
                .groupBy($("liftNo"), $("alarmType"), $("statisticsDate"))
                .select($("liftNo"), $("alarmType").cast(DataTypes.STRING()).as("alarmType"), $("alarmType").count().cast(DataTypes.INT()).as("count"), $("statisticsDate"));

        table.executeInsert("alarmTypeStatisticsKafkaSinkTable");

        try {
            log.error("before sleep.................");
            Thread.sleep(60000*3);
            log.error("after sleep.................");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
