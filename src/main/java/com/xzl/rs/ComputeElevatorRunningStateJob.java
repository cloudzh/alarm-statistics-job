/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xzl.rs;

import com.xzl.rs.function.DynamicTableFunction;
import com.xzl.rs.function.JsonDeserializationScheme;
import com.xzl.rs.function.RunningStateProcessFunction;
import com.xzl.rs.model.LiftAlarmStatistics;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.api.TableResult;

import java.util.Properties;

import static org.apache.flink.table.api.Expressions.$;
import static org.apache.flink.table.api.Expressions.call;

public class ComputeElevatorRunningStateJob {

    public static void main(String[] args) throws Exception {

        EnvironmentSettings settings = EnvironmentSettings.newInstance().useBlinkPlanner().build();
        TableEnvironment tableEnv = TableEnvironment.create(settings);
        tableEnv.executeSql("create table `CalculateTriggerTable` (\n" +
                "`trigger_date` String\n" +
                ")with (\n" +
                "'connector' = 'kafka',\n" +
                "'topic' = 'calculate-trigger-topic',\n" +
                "'properties.group.id' = 'test-consumer-1', \n" +
                "'scan.startup.mode' = 'latest-offset', \n" +
                "'properties.bootstrap.servers' = '101.35.245.42:9092','format'='json' \n" +
                ")");

//        tableEnv.executeSql("CREATE CATALOG alarm_storage WITH(\n" +
//                "    'type' = 'jdbc',\n" +
//                "    'default-database' = 'alarm_info',\n" +
//                "    'username' = 'root',\n" +
//                "    'password' = 'root',\n" +
//                "    'base-url' = 'jdbc:mysql://101.35.245.42:3306/'\n" +
//                ")");

        tableEnv.executeSql("create table trigger_printer(`trigger_date` String null) with ('connector'='print')");
        tableEnv.executeSql("create table printer(`liftNo` String  NULL ,`alarmType` String  NULL) with ('connector'= 'print') ");


        Table kafkaTable = tableEnv.sqlQuery("select trigger_date from CalculateTriggerTable").select($("trigger_date"));
//                .executeInsert("trigger_printer")


        kafkaTable.joinLateral(call(DynamicTableFunction.class, $("trigger_date")))
                .select($("liftNo")).execute();
//                .executeInsert("printer");


//        tableEnv.sqlQuery("select alarmNo,liftNo,alarmType from alarm_storage.alarm_info.cl_alarm_info_20211027")
//                .executeInsert("printer");
//        tableEnv.from("")
//                .select($("")).execute();
//                .createTemporalTableFunction($(""),$(""))
        ;

    }
}
