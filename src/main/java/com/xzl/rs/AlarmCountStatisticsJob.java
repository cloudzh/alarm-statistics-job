package com.xzl.rs;

import com.xzl.rs.function.AlarmAggregateFunction;
import com.xzl.rs.function.AlarmStatisticsProcessWindowFunction;
import com.xzl.rs.function.DynamicTumblingEventTimeWindows;
import com.xzl.rs.msg.AlarmInfo;
import com.xzl.rs.msg.deserialization.AlarmInfoDeserialization;
import com.xzl.rs.msg.sink.AlarmStatistics;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.util.OutputTag;

import java.io.InputStream;
import java.time.Duration;
import java.time.ZoneId;
import java.util.Properties;

public class AlarmCountStatisticsJob {

    public static void main(String[] args) throws Exception {

        InputStream input = AlarmCountStatisticsJob.class.getClassLoader().getResourceAsStream("job.properties");
        ParameterTool tool = ParameterTool.fromPropertiesFile(input);

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
//        env.setRuntimeMode(RuntimeExecutionMode.STREAMING);

        Properties kafkaProps = new Properties();
        kafkaProps.setProperty("bootstrap.servers", tool.get("source.kafka.address"));
        kafkaProps.setProperty("group.id", tool.get("source.kafka.consumer.group-id"));

        FlinkKafkaConsumer<AlarmInfo> kafkaConsumer = new FlinkKafkaConsumer<AlarmInfo>("alarm-monitor-topic", new AlarmInfoDeserialization(), kafkaProps);
        DataStream<AlarmInfo> dataStream = env.addSource(kafkaConsumer);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

//        FlinkKafkaProducer countProducer = new FlinkKafkaProducer(tool.get("target.kafka.address"), "alarm_count_in_minutes_topic", new SimpleStringSchema());
//        FlinkKafkaProducer alarmTypeCountProducer = new FlinkKafkaProducer(tool.get("target.kafka.address"), "alarm_count_per_type_in_minutes_topic", new SimpleStringSchema());
        FlinkKafkaProducer liftAlarmCountProducer = new FlinkKafkaProducer(tool.get("target.kafka.address"), "alarm_count_per_type_lift_in_minute_topic", new SimpleStringSchema());


        final OutputTag<AlarmStatistics> yesterdayStatistics = new OutputTag<AlarmStatistics>("yesterday-statistics") {
        };
        dataStream = dataStream.assignTimestampsAndWatermarks(
                WatermarkStrategy.<AlarmInfo>forBoundedOutOfOrderness(Duration.ofMinutes(1))
                        .withTimestampAssigner((SerializableTimestampAssigner<AlarmInfo>) (element, recordTimestamp) -> element.getAlarmDatetime().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli())
        );

        KeyedStream<AlarmInfo, Integer> keyedStream = dataStream
//                .filter(alarm -> alarm.getAlarmDatetime().isAfter(LocalDateTime.now().minusDays(1)) && alarm.getAlarmDatetime().isBefore(LocalDateTime.now().plusHours(1)))
                .keyBy(alarmInfo -> alarmInfo.getLiftNo().hashCode() + alarmInfo.getAlarmType());

        SingleOutputStreamOperator<AlarmStatistics> soso = keyedStream
//                .window(TumblingEventTimeWindows.of(Time.minutes(5)))
                .window(new DynamicTumblingEventTimeWindows(5))
                .aggregate(new AlarmAggregateFunction(), new AlarmStatisticsProcessWindowFunction(yesterdayStatistics));

        soso
                .addSink(liftAlarmCountProducer).name("AlarmCountPerAlarmTypeAndLiftIn5Min");

        DataStream<AlarmStatistics> sideOutputStream = soso.getSideOutput(yesterdayStatistics);

//        dataStream.keyBy(alarmInfo -> alarmInfo.getAlarmType()).window(TumblingEventTimeWindows.of(Time.minutes(1)))
//                .aggregate(null).addSink(alarmTypeCountProducer).name("AlarmTypeCountIn1Min");

//        dataStream.timeWindowAll(Time.minutes(1)).aggregate(null).addSink(countProducer).name("AlarmCountIn1Min");

        env.execute();

    }
}
