/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xzl.rs;

import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.TableEnvironment;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static com.xzl.rs.ddl.CreateTable.*;
import static org.apache.flink.table.api.Expressions.$;

/**
 * Skeleton for a Flink Batch Job.
 *
 * <p>For a tutorial how to write a Flink batch application, check the
 * tutorials and examples on the <a href="https://flink.apache.org/docs/stable/">Flink Website</a>.
 *
 * <p>To package your application into a JAR file for execution,
 * change the main class in the POM.xml file to this class (simply search for 'mainClass')
 * and run 'mvn clean package' on the command line.
 */
public class BatchJob {

    public static void main(String[] args) throws Exception {
        // set up the batch execution environment
//        final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        String startDateStr = "20211031";

        EnvironmentSettings settings = EnvironmentSettings.newInstance().inBatchMode().build();
        TableEnvironment tableEnv = TableEnvironment.create(settings);

        tableEnv.executeSql(CREATE_STATISTICS_KAFKA_SINK);

        tableEnv.executeSql(CREATE_STATISTICS_PRINTER);

        LocalDate startDate = LocalDate.parse(startDateStr, DateTimeFormatter.ofPattern("yyyyMMdd"));

//        while (startDate.isBefore(endDate)) {

        String tableNameSuffix = startDate.format(DateTimeFormatter.ofPattern("yyyyMMdd"));


        tableEnv.executeSql(String.format(MYSQL_ALARM_SOURCE_TEMPLATE, tableNameSuffix, tableNameSuffix));

        Table table = tableEnv.from("alarmInfo" + tableNameSuffix).select($("liftNo"), $("alarmType"), $("ytStatus"), $("alarmDatetime")).where($("ytStatus").isEqual("20"));

        table = table
                .select($("liftNo"), $("alarmType"), $("alarmDatetime").cast(DataTypes.DATE()).as("statisticsDate"))
                .groupBy($("liftNo"), $("alarmType"), $("statisticsDate"))
                .aggregate($("alarmType").count().as("alarmCount"))
                .select($("liftNo"), $("alarmType").cast(DataTypes.STRING()).as("alarmType"), $("alarmCount").cast(DataTypes.INT()).as("alarmCount"), $("statisticsDate"))
                .filter($("liftNo").isEqual("330371011-7676-2603"))
        ;

        table.executeInsert("alarm_statistics_printer");
        table.executeInsert("alarm_statistics_sink");
        tableEnv.executeSql("drop table alarmInfo" + tableNameSuffix);

    }
}
