package com.xzl.rs;

import com.xzl.rs.function.TransformFunction;
import com.xzl.rs.msg.sink.AlarmStatistics;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.operators.OneInputStreamOperator;
import org.apache.flink.streaming.connectors.kafka.partitioner.FlinkKafkaPartitioner;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

public class CalculateRunningStateJob {

    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);
        createCatalog(tableEnv);
        //kafka connector consumer
        env.fromSource(createConsumer(), WatermarkStrategy.noWatermarks(), "kafkaSource").setParallelism(1)
                .transform("transformToAlarm", TypeInformation.of(AlarmStatistics.class), (OneInputStreamOperator) new TransformFunction<AlarmStatistics>());

        //kafka connector producer

        //mysql connector datasource
        DataStream dataStreamFromMysql = dataStreamForMysql(tableEnv, "20211027");

        DataStream dataStreamFromCatalog = dataStreamFromCatalog(tableEnv, "20211028");
    }

    private static KafkaSource createConsumer() {

        return KafkaSource.builder()
                .setBootstrapServers("101.35.245.42:9092")
                .setTopics("calculate-trigger-topic")
                .setGroupId("test-consumer-1")
                .setStartingOffsets(OffsetsInitializer.latest())
//                .setValueOnlyDeserializer(JsonDeserialize)
                .build();
    }

//    private static KafkaSink createProducer() {
//        return KafkaSink.builder()
//                .setBootstrapServers("101.35.245.42:9092")
//                .setRecordSerializer(
//                        KafkaRecordSerializationSchema.builder()
//                                .setTopic("alarm_type_statistics_topic")
//                                .setPartitioner(new FlinkKafkaPartitioner<Object>() {
//                                    @Override
//                                    public int partition(Object record, byte[] key, byte[] value, String targetTopic, int[] partitions) {
//                                        return 0;
//                                    }
//                                })
//                                .build()
//                )
//                .build();
//    }

    private static DataStream dataStreamForMysql(StreamTableEnvironment tableEnv, String tableNameSuffix) {
        tableEnv.executeSql("CREATE TABLE `alarmInfo" + tableNameSuffix + "` (\n" +
                "  `id` int NOT NULL  ,\n" +
                "  `alarmNo` String NOT NULL ,\n" +
                "  `liftNo` String  NULL ,\n" +
                "  `authNo` String  NULL ,\n" +
                "  `embeddedSn` String  NULL ,\n" +
                "  `standardType` int  NULL ,\n" +
                "  `alarmType` int  NULL,\n" +
                "  `alarmLevel` int  NULL,\n" +
                "  `sleepy` tinyint  NULL,\n" +
                "  `online` tinyint  NULL ,\n" +
                "  `status` tinyint  NULL,\n" +
                "  `createTime` timestamp  NULL ,\n" +
                "  `alarmDatetime` timestamp  NULL ,\n" +
                "  `alarmEndDatetime` timestamp  NULL ,\n" +
                "  `handleDateTime` timestamp  NULL ,\n" +
                "  `handler` String  NULL ,\n" +
                "  `handleStatus` tinyint,\n" +
                "  `ytStatus` String  NULL ,\n" +
                "  `videoStatus` tinyint  ,\n" +
                "  PRIMARY KEY (`id`) NOT ENFORCED\n" +
                ") with (\n" +
                "  'connector' = 'jdbc',\n" +
                "  'url' = 'jdbc:mysql://101.35.245.42:3306/alarm_info',\n" +
                "  'table-name' = 'cl_alarm_info_" + tableNameSuffix + "',\n" +
                "  'username' = 'root',\n" +
                "  'password' = 'root',\n" +
                "  'scan.fetch-size' = '1000', \n" +
//                "  'scan.partition.column' = 'alarmType',\n" +
//                "  'scan.partition.num' = '9',\n" +
                "  'lookup.max-retries' = '3' \n" +
                ")");

        Table table = tableEnv.from("alarmInfo" + tableNameSuffix);

        return tableEnv.toDataStream(table);

    }

    private static void createCatalog(StreamTableEnvironment tableEnv) {
        tableEnv.executeSql("CREATE CATALOG alarm_storage WITH(\n" +
                "    'type' = 'jdbc',\n" +
                "    'default-database' = 'alarm_info',\n" +
                "    'username' = 'root',\n" +
                "    'password' = 'root',\n" +
                "    'base-url' = 'jdbc:mysql://101.35.245.42:3306'\n" +
                ")");
    }

    private static DataStream dataStreamFromCatalog(StreamTableEnvironment tableEnv, String tableSuffix) {

        Table table = tableEnv.sqlQuery("select alarmNo,liftNo,alarmType from alarm_storage.alarm_info.cl_alarm_info_20211027");

        return tableEnv.toDataStream(table);

    }


}
