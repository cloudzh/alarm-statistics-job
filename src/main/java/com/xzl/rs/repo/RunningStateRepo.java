package com.xzl.rs.repo;

import com.xzl.rs.model.AlgorithmConfig;
import com.xzl.rs.model.LiftRunningState;

import java.time.LocalDate;

public class RunningStateRepo {

    private RunningStateConfigRepo configRepo;

    LiftRunningState findByDate(String liftNo,String alarmType, LocalDate date) {
        AlgorithmConfig config = configRepo.getBy(alarmType);

        return new LiftRunningState(liftNo,date,config);
    }
}
